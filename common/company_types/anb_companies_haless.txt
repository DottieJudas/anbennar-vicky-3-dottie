﻿company_concerned_citizens = { #Concerned Citizens for a Greener Haless
	icon = "gfx/interface/icons/company_icons/basic_colonial_plantations_1.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_plantation.dds"

	flavored_company = yes

	building_types = {
		building_rice_farm
		building_silk_plantation
		building_sugar_plantation
	}
	potential = {
		OR = {
			capital.region = sr:region_thidinkai
			capital.region = sr:region_bomdan
			capital.region = sr:region_lupulan
			capital.region = sr:region_kharunyana
			capital.region = sr:region_south_rahen
			capital.region = sr:region_north_rahen
			capital.region = sr:region_west_yanshen
			capital.region = sr:region_east_yanshen
			capital.region = sr:region_north_yanshen
			capital.region = sr:region_nomsyulhan
		}
	}

	possible = {
		OR = {
			capital.region = sr:region_thidinkai
			capital.region = sr:region_bomdan
			capital.region = sr:region_lupulan
			capital.region = sr:region_kharunyana
			capital.region = sr:region_south_rahen
			capital.region = sr:region_north_rahen
			capital.region = sr:region_west_yanshen
			capital.region = sr:region_east_yanshen
			capital.region = sr:region_north_yanshen
			capital.region = sr:region_nomsyulhan
		}
		has_law = law_type:law_industry_banned
	}

	prosperity_modifier = {
		state_birth_rate_mult = 0.05
		state_pollution_reduction_health_mult = 0.1
		building_group_bg_plantations_throughput_add = 0.1 
	}

	ai_weight = {
		value = 3
	}
}