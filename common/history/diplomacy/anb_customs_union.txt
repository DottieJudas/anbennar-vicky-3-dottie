﻿## Customs unions do not exist anymore. The only way to join their market now is to join their (market) power bloc or become their subject.
#DIPLOMACY = {
#	c:A30 = {	#Magocratic Demesne-Wyvernheart customs union as Wyvernheart was helped a lot by them
#		create_diplomatic_pact = {
#			country = c:A26
#			type = customs_union
#		}
#	}
#
#	c:A02 = {	#Vivin
#		create_diplomatic_pact = {	#Ravelian is under stinky Vivin CU. They definitely would rather be in Anbennar's as its cooler and has more stuff
#			country = c:A33
#			type = customs_union
#		}
#	}
#	
#	c:A27 = { #Blademarches
#		create_diplomatic_pact = { # This union is basically required to have both nation be able to function
#			country = c:A25 #Araionn
#			type = customs_union
#		}
#	}
#
#	c:Y24 = {	#Lupulan Alliance
#		create_diplomatic_pact = {
#			country = c:Y25 #Pelimkana
#			type = customs_union
#		}
#		create_diplomatic_pact = {
#			country = c:Y26 #Isdusama
#			type = customs_union
#		}
#		create_diplomatic_pact = {
#			country = c:Y27 #Rihasbren
#			type = customs_union
#		}
#		create_diplomatic_pact = {
#			country = c:Y28 #Tsaesolan
#			type = customs_union
#		}
#		create_diplomatic_pact = {
#			country = c:Y29 #Lelak
#			type = customs_union
#		}
#		create_diplomatic_pact = {
#			country = c:Y30 #Hijoadan
#			type = customs_union
#		}
#		create_diplomatic_pact = {
#			country = c:Y31 #Labanyak
#			type = customs_union
#		}
#	}
#	
#	c:C39 = { #Ameion
#		create_diplomatic_pact = { 
#			country = c:C37 #Deyeion
#			type = customs_union
#		}
#	}
#
#}