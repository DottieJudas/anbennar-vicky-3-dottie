﻿upper_class = {
	social_hierarchy = default_social_hierarchy
	strata = upper

	allowed_professions = {
		aristocrats
		capitalists
		mages
	}
}

middle_class = {
	social_hierarchy = default_social_hierarchy
	strata = middle

	allowed_professions = {
		academics
		bureaucrats
		clergymen
		engineers
		farmers
		officers
		shopkeepers
		adventurers
	}
}

lower_class = {
	social_hierarchy = default_social_hierarchy
	strata = lower

	allowed_professions = {
		clerks
		laborers
		machinists
		peasants
		slaves
		soldiers
	}
}
