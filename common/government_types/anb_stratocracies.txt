﻿gov_stratocracy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_GENERAL"
	female_ruler = "RULER_TITLE_GENERAL"
	
	possible = {
		has_law = law_type:law_stratocracy
		country_has_voting_franchise = no
		OR = {
			has_law = law_type:law_mass_conscription
			has_law = law_type:law_professional_army
		}
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}
